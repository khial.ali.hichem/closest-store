<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Store;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class LocationSearchController extends Controller
{
    //

    public function index(Request $request) {
        // dd($request->longitude, $request->latitude);

        $closest_store = Store::orderBySpatial(
            'positions', 
            Point::fromWKT("POINT($request->latitude $request->longitude)"), 
            'distance',
            'desc'
        )->take(1)->get();

        // dd($closest_store);

        return view('search_result', ["store" => $closest_store[0]]);
    }
}
