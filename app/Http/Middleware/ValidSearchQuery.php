<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidSearchQuery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->has('q')) {
            return $next($request);
        }

        return redirect('/')->withInput();
    }
}
