<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Store extends Model
{
    use HasFactory, SpatialTrait;

    protected $fillable = [
        'name',
        'address'
    ];

    protected $spatialFields = [
        'positions'
    ];

}
